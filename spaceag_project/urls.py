from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from workers import views

urlpatterns = [
    path('field_workers/', views.WorkerList.as_view(), name='list_create'),
    path('field_workers/<int:pk>', views.WorkerDetail.as_view(), name='detail_edit_delete'),
]

urlpatterns = format_suffix_patterns(urlpatterns)