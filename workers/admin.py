from django.contrib import admin

# Register your models here.

from .models import Worker

class WorkerAdmin(admin.ModelAdmin):
    fieldsets = [
        ('First Name', {'fields': ['first_name']}),
        ('Last Name', {'fields': ['last_name']}),
        ('Function', {'fields': ['function']}),
        ('Created At', {'fields': ['created_at']}),
    ]
    list_display = ('first_name', 'last_name', 'function', 'created_at')
    list_filter = ['first_name', 'last_name', 'function']
    search_fields = ['first_name', 'last_name', 'function', 'created_at']

admin.site.register(Worker, WorkerAdmin)