from django.db import models

# Create your models here.

FUNCTION_CHOICES = (
	('harvest', 'Harvest'),
	('pruning', 'Pruning'),
	('scouting', 'Scouting'),
	('other', 'Other'),
)

class Worker(models.Model):
    first_name = models.CharField(max_length=80)
    last_name = models.CharField(max_length=80)
    function = models.CharField(max_length=8, choices=FUNCTION_CHOICES, default='harvest')
    created_at = models.DateField()

    class Meta:
        ordering = ['first_name']
    
    def __str__(self):
        return self.first_name + self.last_name