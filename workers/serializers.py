from .models import Worker
from rest_framework import serializers
from datetime import date

class WorkerSerializer(serializers.ModelSerializer):
	first_name = serializers.CharField(required=True)
	last_name = serializers.CharField(required=True)
	created_at = serializers.DateField(default=date.today)

	class Meta:
		model = Worker
		fields = ['id', 'first_name', 'last_name', 'function', 'created_at']

class WorkerPUSerializer(serializers.ModelSerializer):
	first_name = serializers.CharField()
	last_name = serializers.CharField()
	created_at = serializers.DateField()

	class Meta:
		model = Worker
		fields = ['first_name', 'last_name', 'function', 'created_at']
