import pytest

from .models import Worker
from django.urls import reverse

@pytest.fixture
def fixture_worker(db):
	return Worker.objects.create(
		first_name='Julia',
		last_name='Roberts',
		function='pruning',
		created_at='2021-10-02',
	)

@pytest.mark.django_db
def test_list_create_view(client):
	url = reverse('list_create')
	response = client.get(url)
	assert response.status_code == 200

@pytest.mark.django_db
def test_detail_edit_delete_view(client, fixture_worker):
	worker = Worker.objects.create(
		first_name='Julia',
		last_name='Roberts',
		function='pruning',
		created_at='2021-10-02',
	)
	url = reverse('detail_edit_delete', kwargs={'pk': fixture_worker.id})
	response = client.get(url)
	assert response.status_code == 200

@pytest.fixture
def api_client():
	from rest_framework.test import APIClient
	return APIClient()

@pytest.mark.django_db
def test_list_API_route(api_client, fixture_worker):
	response = api_client.get('/field_workers/', format='json')
	assert response.status_code == 200

@pytest.mark.django_db
def test_create_API_route(api_client, fixture_worker):
	response = api_client.post('/field_workers/', {
		'first_name': 'Calvin',
		'last_name': 'Klane',
		'function': 'other',
		'created_at': '2021-03-12',
	}, format='json')
	assert response.status_code == 201

@pytest.mark.django_db
def test_detail_API_route(api_client, fixture_worker):
	response = api_client.get('/field_workers/', format='json', pk=fixture_worker.id)
	assert response.status_code == 200

@pytest.mark.django_db
def test_edit_API_route(api_client, fixture_worker):
	response = api_client.post('/field_workers/', {
		'first_name': 'Calvin',
		'last_name': 'Klane',
	}, format='json', pk=fixture_worker.id)
	assert response.status_code == 201

@pytest.mark.django_db
def test_delete_API_route(api_client, fixture_worker):
	url = reverse('detail_edit_delete', kwargs={'pk': fixture_worker.id})
	assert api_client.delete(url).status_code == 204