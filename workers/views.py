from .models import Worker
from .serializers import WorkerSerializer, WorkerPUSerializer
from rest_framework import generics

class WorkerList(generics.ListCreateAPIView):
	queryset = Worker.objects.all()
	serializer_class = WorkerSerializer

class WorkerDetail(generics.RetrieveUpdateDestroyAPIView):
	queryset = Worker.objects.all()
	serializer_class = WorkerPUSerializer

	def put(self, request, *args, **kwargs):
		return self.partial_update(request, *args, **kwargs)